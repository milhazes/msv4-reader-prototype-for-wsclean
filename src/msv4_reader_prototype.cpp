#include "msv4_reader_prototype.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <random>

using namespace tensorstore;

TensorStoreManager::TensorStoreManager(const std::string& filePath)
: _filePath(filePath)
{
    attributesInfo = _getJson(".zattrs");
    metadataInfo = _getJson(".zmetadata");
}

tensorstore::internal_future::ResultType<tensorstore::SharedOffsetArray<void>> TensorStoreManager::ReadArrayMetadata(const std::string metadataTag) {

    auto start = std::chrono::high_resolution_clock::now();

    auto context = tensorstore::Context::Default();
    _openFuture = tensorstore::Open(
        { { "driver", "zarr" },
            { "kvstore", { { "driver", "file" }, { "path", _filePath + "/" + metadataTag } } }
        },
            context,
            tensorstore::OpenMode::open,
            tensorstore::RecheckCached{ false },
            tensorstore::ReadWriteMode::read);

    auto result = _openFuture.result();

    tensorstore::IndexDomain domain;
    tensorstore::DataType dtype;

    if (result.ok())
    {
        auto store = result.value();
        domain = store.domain();
        auto shapeSpan = store.domain().shape();
        std::vector<int64_t> shape(shapeSpan.begin(), shapeSpan.end());
        dtype = store.dtype();
    }
    else
        std::cout << "status BAD\n" << result.status();

    auto arrayResult = tensorstore::Read(_openFuture.result().value()).result();

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Read duration: " << std::chrono::duration<double>(end - start).count() << " seconds" << std::endl;

    return arrayResult;
}

::nlohmann::json TensorStoreManager::_getJson(std::string jsonFileName)
{
    // JSON uses a separate driver
    auto attrs_store = tensorstore::Open<::nlohmann::json, 0>(
        { { "driver", "json" },
            { "kvstore", { { "driver", "file" }, { "path", _filePath + "/" + jsonFileName } } } })
        .result()
        .value();

    // Sets attrs_array to a rank-0 array of ::nlohmann::json
    auto attrs_array_result = tensorstore::Read(attrs_store).result();

    ::nlohmann::json attrs;
    if (attrs_array_result.ok())
    {
        attrs = attrs_array_result.value()();
    }
    else if (absl::IsNotFound(attrs_array_result.status()))
    {
        attrs = ::nlohmann::json::object_t();
    }
    else
    {
        std::cout << "Error: " << attrs_array_result.status();
    }
    
    return attrs;
};

int main(int argc, char *argv[])
{
    std::string filePath = argv[1];

    TensorStoreManager manager(filePath);

    ::nlohmann::json attributes = manager.attributesInfo;
    std::cout << attributes.dump(3) << std::endl;
    std::cout << "\n\n";

    ::nlohmann::json metadata = manager.metadataInfo;
    std::cout << metadata.dump(3) << std::endl;
    std::cout << "\n\n";

    auto timeArray = manager.ReadArrayMetadata("time");
    std::cout << "time:\n" << timeArray.value() << "\n\n";

    auto baselineAntennaOneArray = manager.ReadArrayMetadata("baseline_antenna1_id");
    std::cout << "baseline_antenna1_id:\n" << baselineAntennaOneArray.value() << "\n\n";

    auto baselineAntennaTwoArray = manager.ReadArrayMetadata("baseline_antenna2_id");
    std::cout << "baseline_antenna2_id:\n" << baselineAntennaTwoArray.value() << "\n\n";

    auto baselineIDArray = manager.ReadArrayMetadata("baseline_id");
    std::cout << "baseline_id:\n" << baselineIDArray.value() << "\n\n";

    auto frequencyArray = manager.ReadArrayMetadata("frequency");
    std::cout << "frequency:\n" << frequencyArray.value() << "\n\n";

    //auto polarizationArray = manager.ReadArrayMetadata("polarization");
    //std::cout << "polarization:\n" << polarizationArray.value() << "\n\n";

    //auto UVWLabelArray = manager.ReadArrayMetadata("uvw_label");
    //std::cout << "uvw_label:\n" << UVWLabelArray.value() << "\n\n";

    auto UVWArray = manager.ReadArrayMetadata("UVW");
    std::cout << "UVW:\n" << UVWArray.value() << "\n\n";
    //std::cout << "UVW:\n" << UVWArray.value()[10][20] << "\n\n";

    auto VisibilitiesArray = manager.ReadArrayMetadata("VISIBILITY");
    std::cout << "VISIBILITY:\n" << UVWArray.value() << "\n\n";

    return 0;
}