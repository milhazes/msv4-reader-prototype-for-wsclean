#include <absl/flags/flag.h>
#include <absl/status/status.h>
#include <absl/strings/match.h>
#include <absl/strings/str_join.h>
#include <absl/flags/parse.h>
#include <nlohmann/json.hpp>
#include "tensorstore/array.h"
#include "tensorstore/context.h"
#include "tensorstore/data_type.h"
#include "tensorstore/index.h"
#include "tensorstore/index_space/dim_expression.h"
#include "tensorstore/index_space/index_transform.h"
#include "tensorstore/open.h"
#include "tensorstore/open_mode.h"
#include "tensorstore/spec.h"
#include "tensorstore/strided_layout.h"
#include "tensorstore/tensorstore.h"
#include "tensorstore/util/json_absl_flag.h"
#include "tensorstore/util/result.h"
#include "tensorstore/util/span.h"
#include "tensorstore/util/status.h"
#include "tensorstore/util/str_cat.h"
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>

using namespace tensorstore;

class TensorStoreManager
{
    public:

    /* Class that reads atributes and multidimentional arrays from a Zarr store using Tensorstore */
    TensorStoreManager(const std::string& filePath);

    ::nlohmann::json attributesInfo;
    ::nlohmann::json metadataInfo;

    /* Reads metadata and stores in a multidimentional array */
    tensorstore::internal_future::ResultType<tensorstore::SharedOffsetArray<void>> ReadArrayMetadata(const std::string metadataTag);
    
    private:

    std::string _filePath;
    tensorstore::Future<tensorstore::TensorStore<>> _openFuture;
    ::nlohmann::json _getJson(std::string jsonFileName);
};